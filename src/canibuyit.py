import telebot
import json
from telebot import types, apihelper
import yfinance as yf
from datetime import date, timedelta


token = '1229581452:AAFGPtewhtidCCH-088o_dP_qNLJH3ADGnc'

#apihelper.proxy = {'https': 'socks5h://levelup:cAApT@eW@3.22.208.69:2323'}

bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def start(message):
    #markup = types.InlineKeyboardMarkup()
    #markup.add(types.InlineKeyboardButton(text='Yandex', callback_data=1))
    #markup.add(types.InlineKeyboardButton(text='Google', callback_data=2))
    markup = types.ForceReply()
    send_mess_start = f"<b>Привет! {message.from_user.first_name}</b>!\nО какой акции узнаем сегодня?\nНапиши мне тикер"
    bot.send_message(message.chat.id, send_mess_start, parse_mode='html', reply_markup=markup)


@bot.message_handler()
def get_ticker_name(message):
    ticker_name = str.upper(message.text)
    try:
        data = get_ticker_info(ticker_name)
        bot.reply_to(message, parse_ticker_info(data))
    except Exception:
        bot.reply_to(message, 'Что-то пошло не так :(\nПроверь тикер')
#@bot.callback_query_handler(func=lambda call: True)
#def query_handler(call):
#    bot.answer_callback_query(callback_query_id=call.id, text='Краткая информация')
#    if call.data == '1':
#        answer = 'Российская компания Yandex'
#        data = get_stock_info('YNDX')
#    elif call.data == '2':
#        answer = 'Американская компания Google'
#        data = get_stock_info('GOOGL')
#    bot.send_message(call.message.chat.id, parse_stock_info(data))


def get_ticker_info(ticker_name):
    #date_to = date.today()
    #date_from = date_to - timedelta.days(30)
    #data = yf.download(stock_name, date_from, date_to)
    ticker = yf.Ticker(ticker_name)
    return ticker.info


def parse_ticker_info(ticker_info):
    answer_string = f'Name: {ticker_info["shortName"]}\nBid: {ticker_info["bid"]}'
    if (ticker_info["dividendYield"] != None):
        #print(True)
        answer_string = 'Можно купить на долгосрок, есть дивиденды\n' + answer_string
    else:
        answer_string = 'Дивидендов нет :(\n' + answer_string
    if (float(ticker_info["forwardPE"]) < 3.0) and (float(ticker_info["regularMarketPrice"]) < float(ticker_info["bid"])):
        answer_string = 'Можно попробовать спекулировать, цена занижена\n' + answer_string + f'\nforw P/E: {ticker_info["forwardPE"]}\nmarket price: {ticker_info["regularMarketPrice"]}'
    else:
        answer_string = 'Цена справедлива или завышена :(\n' + answer_string + f'\nforw P/E: {ticker_info["forwardPE"]}\nmarket price: {ticker_info["regularMarketPrice"]}'
    print(answer_string)
    return answer_string


if __name__ == '__main__':
    print('Starting bot...')
    bot.polling(none_stop=True, interval=0)
